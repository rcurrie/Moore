###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of minimum bias lines.

Each line has a very minimal set of restrictions on what must be present in the
event, such that the selected events are biased in a very limited way,
typically meant with respect to 'heavy flavour events'.
"""
import cppyy

from Configurables import VoidFilter as _VoidFilter

from PyConf.Algorithms import DeterministicPrescaler
from PyConf.application import make_odin
from PyConf.components import make_algorithm
from Moore.config import HltLine

from RecoConf.hlt1_tracking import make_hlt1_tracks

from Functors import EVENTTYPE, SIZE
from Functors import math as fmath


def odin_nobias_filter(make_odin=make_odin):
    nobias_bit = cppyy.gbl.LHCb.ODIN.NoBias

    def input_transform(InputLocation):
        return {"Cut": fmath.test_bit(EVENTTYPE(InputLocation), nobias_bit)}

    NoBiasEventFilter = make_algorithm(
        _VoidFilter, input_transform=input_transform)

    odin = make_odin()
    return NoBiasEventFilter(InputLocation=odin)


def low_multiplicity_velo_line(name="Hlt1MicroBiasLowMultVeloLine",
                               prescale=1,
                               nvelo_tracks_min=5):
    """Accepts NoBias events with at least some number of VELO tracks."""

    def input_transform(InputLocation):
        return {"Cut": SIZE(InputLocation) > nvelo_tracks_min}

    ContainerSizeFilter = make_algorithm(
        _VoidFilter, input_transform=input_transform)

    nobias = odin_nobias_filter()
    velo_tracks = make_hlt1_tracks()["Velo"]["Pr"]
    ntracks_filter = ContainerSizeFilter(InputLocation=velo_tracks)

    return HltLine(name=name, algs=[nobias, ntracks_filter], prescale=prescale)


def no_bias_line(name="Hlt1NoBiasLine", prescale=1):
    """Accepts all crossings marked with the NoBias ODIN bit."""
    nobias = odin_nobias_filter()

    return HltLine(
        name=name,
        algs=[nobias],
        prescale=prescale,
    )
