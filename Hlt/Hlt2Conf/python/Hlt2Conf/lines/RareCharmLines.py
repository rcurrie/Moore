###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of Rare Charm HLT2 lines.


Final states that are built are:

--------------------
two-body lines:
--------------------

D*+ -> D0(-> mu+ mu-) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02MuMuLine]
D*+ -> D0(-> mu+ e-) pi+ cc and D*+ -> (D0 -> mu- e+) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02MuELine]
D*+ -> D0(-> e+ e-) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02EELine]

control lines

D*+ -> D0(-> pi- pi+) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02PiPiLine]
D*+ -> D0(->K- pi+ ) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KPiLine]
D*+ -> D0(-> K- mu+ ) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KMuLine]

--------------------
three-body lines:
--------------------

D+ -> pi+ mu+ mu- cc (OS) [Hlt2RareCharmD2PiMuMuOSLine]
D+ -> pi- mu+ mu+ cc (SS) [Hlt2RareCharmD2PiMuMuSSLine]
D+ -> pi+ mu+ mu+ cc (WS) [Hlt2RareCharmD2PiMuMuWSLine]

D+ -> pi+ e+ e- cc (OS) [Hlt2RareCharmD2PiEEOSLine]
D+ -> pi- e+ e+ cc (SS) [Hlt2RareCharmD2PiEESSLine]
D+ -> pi+ e+ e+ cc (WS) [Hlt2RareCharmD2PiEEWSLine]

D+ -> K+ mu+ mu- cc (OS) [Hlt2RareCharmD2KMuMuOSLine]
D+ -> K- mu+ mu+ cc (SS) [Hlt2RareCharmD2KMuMuSSLine]
D+ -> K+ mu+ mu+ cc (WS) [Hlt2RareCharmD2KMuMuWSLine]

D+ -> K+ e+ e- cc (OS) [Hlt2RareCharmD2KEEOSLine]
D+ -> K- e+ e+ cc (SS) [Hlt2RareCharmD2KEESSLine]
D+ -> K+ e+ e+ cc (WS) [Hlt2RareCharmD2KEEWSLine]

D+ -> pi+ mu+ e- cc (OS) [Hlt2RareCharmD2KMuEOSLine]
D+ -> pi+ e+ mu- cc (OS) [Hlt2RareCharmD2KEMuOSLine]
D+ -> pi- mu+ e+ cc (SS) [Hlt2RareCharmD2KMuESSLine]
D+ -> pi+ mu+ e+ cc (WS) [Hlt2RareCharmD2KMuEWSLine]

D+ -> K+ mu+ e- cc (OS) [Hlt2RareCharmD2KMuESSLine]
D+ -> K+ e+ mu- cc (OS) [Hlt2RareCharmD2KMuEWSLine]
D+ -> K- mu+ e+ cc (SS) [Hlt2RareCharmD2PiMuEOSLine]
D+ -> K+ mu+ e+ cc (WS) [Hlt2RareCharmD2KMuEWSLine]


--------------------
four-body lines:
--------------------

Muon lines

D*+ -> D0 (-> pi+ pi- mu+ mu-) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02PiPiMuMuLine]
D*+ -> D0 (-> K+ K- mu+ mu-) pi+ cc   [Hlt2RareCharmDstp2D0Pip_D02KKMuMuLine
D*+ -> D0 (-> K- pi+ mu+ mu-)pi+ cc and  D*+ -> D0 (-> K+ pi- mu+ mu-)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KPiMuMuLine]
D0 -> K- pi+ mu+ mu- cc [Hlt2RareCharmD02KPiMuMuUntagLine]

Electron lines

D*+ -> D0( -> pi+ pi- e+ e-) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02PiPiEELine]
D*+ -> D0( -> K+ K- e+ e-) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KKEELine]
D*+ -> D0( -> K- pi+ e+ e-) pi+ cc and D*+ -> (D0 -> K+ pi- e+ e-) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KPiEELine]

LFV decays

D*+ -> D0 (-> pi+ pi- mu+ e-)pi+ cc, D*+ -> D0 (-> pi+ pi- mu- e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02PiPiMuELine]
D*+ -> D0 (-> K+ K- mu+ e-) pi+ cc, D*+ -> D0 (-> K+ K- mu- e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KKMuELine]
D*+ -> D0 (-> K- pi+ mu+ e-)pi+ cc and D*+ -> D0 (-> K- pi+ mu- e+)pi+ cc and D*+ -> D0 (-> K+ pi- mu+ e-)pi+ cc and  D*+ -> D0 (-> K+ pi- mu- e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KPiMuELine]

LNV decays

D*+ -> D0 (-> pi+ pi+ mu- mu-)pi+ cc and D*+ -> D0 (-> pi- pi- mu+ mu+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02PiPiMuMuSSLine]
D*+ -> D0 (-> K+ K+ mu- mu-)pi+ cc and D*+ -> D0 (-> K- K- mu+ mu+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KKMuMuSSLine]
D*+ -> D0 (-> K+ pi+ mu- mu-)pi+ cc and D*+ -> D0 (-> K- pi- mu+ mu+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KPiMuMuSSLine]

D*+ -> D0 (-> pi+ pi+ e- e-)pi+ cc and D*+ -> D0 (-> pi- pi- e+ e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02PiPiEESSLine]
D*+ -> D0 (-> K+ K+ e- e-)pi+ cc and D*+ -> D0 (-> K- K- e+ e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KKEESSLine]
D*+ -> D0 (-> K+ pi+ e- e-)pi+ cc and D*+ -> D0 (-> K- pi- e+ e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KPiEESSLine]

LFV+ LNV decays

D*+ -> D0 (-> pi+ pi+ mu- e-)pi+ cc and D*+ -> D0 (-> pi- pi- mu+ e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02PiPiMuESSLine]
D*+ -> D0 (-> K+ K+ mu- e-)pi+ cc and D*+ -> D0 (-> K- K- mu+ e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KKMuESSLine]
D*+ -> D0 (-> K+ pi+ mu- e-)pi+ cc and D*+ -> D0 (-> K- pi- mu+ e+)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KPiMuESSLine]

control lines

D*+ -> D0 (-> pi+ pi- pi+ pi-) pi+ cc [Hlt2RareCharmDstp2D0Pip_D02PiPiPiPiControlLine]
D*+ -> D0 (-> K+ K- pi+ pi-) pi+ cc   [Hlt2RareCharmDstp2D0Pip_D02KKPiPiControlLine]
D*+ -> D0 (-> K- pi+ pi+ pi-)pi+ cc and  D*+ -> D0 (-> K+ pi- pi+ pi-)pi+ cc [Hlt2RareCharmDstp2D0Pip_D02KPiPiPiControlLine]

--------------------
baryonic lines:
--------------------

Lambda_c+ -> mu+ mu- p+ , Lambda_c~- -> mu+ mu- p~- [Hlt2RareCharmLc2PMuMuLine]
Lambda_c+ -> mu+ mu+ p~- , Lambda_c~- -> mu- mu- p+ [Hlt2RareCharmLc2PMuMuSSLine]
Lambda_c+ -> mu+ e- p+ , Lambda_c~- -> mu+ e- p~-, Lambda_c+ ->e+ mu-  p+ , Lambda_c~- -> e+ mu- p~- [Hlt2RareCharmLc2PMuELine]
Lambda_c+ -> e+ e- p+ , Lambda_c~- -> e+ e- p~- [Hlt2RareCharmLc2PEELine]
Lambda_c+ -> e+ e+ p~- , Lambda_c~- ->  e- e- p+ [Hlt2RareCharmLc2PEESSLine]

--------------------
"""

from .RareCharmMakers import *

_Mass_M_MIN_Control_HHll = 1765.0 * MeV
_Mass_M_MAX_Control_HHll = 1965.0 * MeV
_DMassWin_LL = 70. * MeV  # MeV

all_lines = {}
"""
************************************************
Definition of the lines
************************************************
"""
""" Define the tagged D02LL lines """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2mumu_line(name='Hlt2RareCharmDstp2D0Pip_D02MuMuLine',
                                  prescale=1):
    pvs = make_pvs()
    muons = make_rarecharm_muons_forTwoBodyDecays()
    dzeros = make_rarecharm_d02ll_dzeros(
        particles=[muons], descriptors=['D0 -> mu- mu+'], pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        #        descriptors=['[D*(2010)+ -> D0 pi+]cc'],
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2mue_line(name='Hlt2RareCharmDstp2D0Pip_D02MuELine',
                                 prescale=1):
    pvs = make_pvs()
    electrons = make_rarecharm_electrons_forTwoBodyDecays()
    muons = make_rarecharm_muons_forTwoBodyDecays()
    dzeros = make_rarecharm_d02ll_dzeros(
        particles=[electrons, muons],
        descriptors=['D0 -> mu- e+', 'D0 -> mu+ e-'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2ee_line(name='Hlt2RareCharmDstp2D0Pip_D02EELine',
                                prescale=1):
    pvs = make_pvs()
    electrons = make_rarecharm_electrons_forTwoBodyDecays()
    dzeros = make_rarecharm_d02ll_dzeros(
        particles=[electrons], descriptors=['D0 -> e- e+'], pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


""" Define the tagged D02HHMUMU lines """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimumu_line(
        name='Hlt2RareCharmDstp2D0Pip_D02PiPiMuMuLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    muons = make_rarecharm_muons()
    dimuons = make_rarecharm_TwoMuons(
        particles=muons, pvs=pvs)  # the dimuon objects
    dzeros = make_rarecharm_d02hhmumu_dzeros(
        particles=[pions, dimuons],
        descriptors=['D0 -> pi- pi+ J/psi(1S)'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimumu_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KPiMuMuLine', prescale=1):

    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    pions = make_rarecharm_pions()
    muons = make_rarecharm_muons()  # the dimuon objects
    dimuons = make_rarecharm_TwoMuons(
        particles=muons, pvs=pvs)  # the dimuon objects
    dzeros = make_rarecharm_d02hhmumu_dzeros(
        particles=[kaons, pions, dimuons],
        #        descriptors=['[D0 -> K- pi+ J/psi(1S)]cc','[D0 -> K+ pi- J/psi(1S)]cc'],
        descriptors=['D0 -> K- pi+ J/psi(1S)', 'D0 -> K+ pi- J/psi(1S)'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        #        descriptors=['[D*(2010)+ -> D0 pi+]cc'],
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dzero2kpimumuuntagged_line(name='Hlt2RareCharmD02KPiMuMuUntagLine',
                               prescale=1):

    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    pions = make_rarecharm_pions()
    muons = make_rarecharm_muons()
    dimuons = make_rarecharm_TwoMuons(
        particles=muons, pvs=pvs)  # the dimuon objects
    dzeros = make_rarecharm_d02hhmumu_dzeros(
        particles=[kaons, pions, dimuons],
        descriptors=['[D0 -> K- pi+ J/psi(1S)]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dzeros],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmumu_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KKMuMuLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    muons = make_rarecharm_muons()  # the dimuon objects
    dimuons = make_rarecharm_TwoMuons(particles=muons, pvs=pvs)
    dzeros = make_rarecharm_d02hhmumu_dzeros(
        particles=[kaons, dimuons],
        descriptors=['D0 -> K- K+ J/psi(1S)'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


"""four-body electron lines"""


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipiee_line(
        name='Hlt2RareCharmDstp2D0Pip_D02PiPiEELine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    electrons = make_rarecharm_electrons()
    dielectrons = make_rarecharm_TwoElectrons(particles=electrons, pvs=pvs)
    dzeros = make_rarecharm_d02hhel_dzeros(
        particles=[pions, dielectrons],
        descriptors=['D0 -> pi- pi+ J/psi(1S)'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpiee_line(name='Hlt2RareCharmDstp2D0Pip_D02KPiEELine',
                                   prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    dielectrons = make_rarecharm_TwoElectrons(particles=electrons, pvs=pvs)
    dzeros = make_rarecharm_d02hhel_dzeros(
        particles=[kaons, pions, dielectrons],
        descriptors=['D0 -> K- pi+ J/psi(1S)', 'D0 -> K+ pi- J/psi(1S)'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkee_line(name='Hlt2RareCharmDstp2D0Pip_D02KKEELine',
                                  prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    dielectrons = make_rarecharm_TwoElectrons(particles=electrons, pvs=pvs)
    dzeros = make_rarecharm_d02hhel_dzeros(
        particles=[kaons, dielectrons],
        descriptors=['D0 -> K- K+ J/psi(1S)'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


"""LFV decays """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimue_line(
        name='Hlt2RareCharmDstp2D0Pip_D02PiPiMuELine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    electrons = make_rarecharm_electrons()
    muons = make_rarecharm_muons()
    electronmuon = make_rarecharm_ElectronMuon(
        particles=[electrons, muons], pvs=pvs)
    dzeros = make_rarecharm_d02hhel_dzeros(
        particles=[pions, electronmuon],
        descriptors=['D0 -> pi- pi+ J/psi(1S)', 'D0 -> pi- pi+ phi(1020)'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmue_line(name='Hlt2RareCharmDstp2D0Pip_D02KKMuELine',
                                   prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    muons = make_rarecharm_muons()
    electronmuon = make_rarecharm_ElectronMuon(
        particles=[electrons, muons], pvs=pvs)
    dzeros = make_rarecharm_d02hhel_dzeros(
        particles=[kaons, electronmuon],
        descriptors=['D0 -> K- K+ J/psi(1S)', 'D0 -> K- K+ phi(1020)'],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimue_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KPiMuELine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    pions = make_rarecharm_pions()
    electrons = make_rarecharm_electrons()
    muons = make_rarecharm_muons()
    electronmuon = make_rarecharm_ElectronMuon(
        particles=[electrons, muons], pvs=pvs)
    dzeros = make_rarecharm_d02hhel_dzeros(
        particles=[kaons, pions, electronmuon],
        descriptors=[
            'D0 -> K- pi+ J/psi(1S)', 'D0 -> K- pi+ phi(1020)',
            'D0 -> K+ pi- J/psi(1S)', 'D0 -> K+ pi- phi(1020)'
        ],
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


""" LNV decays"""


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimumuss_line(
        name='Hlt2RareCharmDstp2D0Pip_D02PiPiMuMuSSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    muons = make_rarecharm_muons()
    pionmuon = make_rarecharm_PionMuon(particles=[pions, muons], pvs=pvs)
    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[pionmuon],
        descriptors=['D0 -> B0 B0', 'D0 -> B~0 B~0'
                     ],  # = D0 -> pi+mu- pi+mu-','D0 -> pi-mu+ pi-mu+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmumuss_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KKMuMuSSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    muons = make_rarecharm_muons()
    kaonmuon = make_rarecharm_KaonMuon(particles=[kaons, muons], pvs=pvs)
    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[kaonmuon],
        descriptors=['D0 -> B0L B0L', 'D0 -> B0H B0H'
                     ],  # = D0 -> K+mu- K+mu-','D0 -> K-mu+ K-mu+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimumuss_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KPiMuMuSSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    pions = make_rarecharm_pions()
    muons = make_rarecharm_muons()
    kaonmuon = make_rarecharm_KaonMuon(particles=[kaons, muons], pvs=pvs)
    pionmuon = make_rarecharm_PionMuon(particles=[pions, muons], pvs=pvs)
    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[kaonmuon, pionmuon],
        descriptors=['D0 -> B0 B0L ', 'D0 -> B~0 B0H'
                     ],  # = D0 -> pi+mu- K+mu-','D0 -> pi-mu+ K-mu+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipieess_line(
        name='Hlt2RareCharmDstp2D0Pip_D02PiPiEESSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    electrons = make_rarecharm_electrons()
    pionelectron = make_rarecharm_PionElectron(
        particles=[pions, electrons], pvs=pvs)
    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[pionelectron],
        descriptors=['D0 -> B_s0 B_s0', 'D0 -> B_s~0 B_s~0'
                     ],  # = D0 -> pi+e- pi+e-','D0 -> pi-e+ pi-e+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkeess_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KKEESSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    kaonelectron = make_rarecharm_KaonElectron(
        particles=[kaons, electrons], pvs=pvs)
    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[kaonelectron],
        descriptors=['D0 -> B_s0L B_s0L', 'D0 -> B_s0H B_s0H'
                     ],  # = D0 -> K+e- K+e-','D0 -> K-e+ K-e+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpieess_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KPiEESSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    kaonelectron = make_rarecharm_KaonElectron(
        particles=[kaons, electrons], pvs=pvs)
    pionelectron = make_rarecharm_PionElectron(
        particles=[pions, electrons], pvs=pvs)
    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[kaonelectron, pionelectron],
        descriptors=['D0 -> B_s0 B_s0L', 'D0 -> B_s~0 B_s0H'
                     ],  # = D0 -> pi+e- K+e-','D0 -> pi-e+ K-e+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


""" LN + LFV decays  """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipimuess_line(
        name='Hlt2RareCharmDstp2D0Pip_D02PiPiMuESSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    electrons = make_rarecharm_electrons()
    pionelectron = make_rarecharm_PionElectron(
        particles=[pions, electrons], pvs=pvs)

    muons = make_rarecharm_muons()
    pionmuon = make_rarecharm_PionMuon(particles=[pions, muons], pvs=pvs)

    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[pionelectron, pionmuon],
        descriptors=['D0 -> B0 B_s0', 'D0 -> B~0 B_s~0'
                     ],  # = D0 -> pi+mu- pi+e-','D0 -> pi-mu+ pi-e+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkmuess_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KKMuESSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    kaonelectron = make_rarecharm_KaonElectron(
        particles=[kaons, electrons], pvs=pvs)
    muons = make_rarecharm_muons()
    kaonmuon = make_rarecharm_KaonMuon(particles=[kaons, muons], pvs=pvs)
    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[kaonelectron, kaonmuon],
        descriptors=['D0 -> B0L B_s0L', 'D0 -> B0H B_s0H'
                     ],  # = D0 -> K+mu- K+e-','D0 -> K-mu+ K-e+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpimuess_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KPiMuESSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    muons = make_rarecharm_muons()
    kaonmuon = make_rarecharm_KaonMuon(particles=[kaons, muons], pvs=pvs)
    electrons = make_rarecharm_electrons()
    pions = make_rarecharm_pions()
    pionelectron = make_rarecharm_PionElectron(
        particles=[pions, electrons], pvs=pvs)
    dzeros = make_rarecharm_d02hhll_sslepton_dzeros(
        particles=[kaonmuon, pionelectron],
        descriptors=['D0 -> B0 B_s0L', 'D0 -> B~0 B_s0H'
                     ],  # = D0 -> pi+mu- K+e-','D0 -> pi-mu+ K-e+'
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+',
                     'D*(2010)- -> D0 pi-'],  ##todo check!
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


"""three body lines """


# Instantiations, PiMuMu
@register_line_builder(all_lines)
@configurable
def d2pimumuos_line(name='Hlt2RareCharmD2PiMuMuOSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, muons],
        descriptors=['[D+ -> pi+ mu+ mu-]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pimumuss_line(name='Hlt2RareCharmD2PiMuMuSSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, muons],
        descriptors=['[D+ -> pi- mu+ mu+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pimumuws_line(name='Hlt2RareCharmD2PiMuMuWSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, muons],
        descriptors=['[D+ -> pi+ mu+ mu+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


# Instantiations, PiEE


@register_line_builder(all_lines)
@configurable
def d2pieeos_line(name='Hlt2RareCharmD2PiEEOSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, electrons],
        descriptors=['[D+ -> pi+ e+ e-]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pimeess_line(name='Hlt2RareCharmD2PiEESSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, electrons],
        descriptors=['[D+ -> pi- e+ e+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pimeews_line(name='Hlt2RareCharmD2PiEEWSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, electrons],
        descriptors=['[D+ -> pi+ e+ e+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


# Instantiations, KMuMu
@register_line_builder(all_lines)
@configurable
def d2kmumuos_line(name='Hlt2RareCharmD2KMuMuOSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, muons],
        descriptors=['[D+ -> K+ mu+ mu-]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2kmumuss_line(name='Hlt2RareCharmD2KMuMuSSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, muons],
        descriptors=['[D+ -> K- mu+ mu+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pimumuws_line(name='Hlt2RareCharmD2KMuMuWSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, muons],
        descriptors=['[D+ -> K+ mu+ mu+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


# Instantiations, KEE
@register_line_builder(all_lines)
@configurable
def d2keeos_line(name='Hlt2RareCharmD2KEEOSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, electrons],
        descriptors=['[D+ -> K+ e+ e-]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2keess_line(name='Hlt2RareCharmD2KEESSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, electrons],
        descriptors=['[D+ -> K- e+ e+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pieews_line(name='Hlt2RareCharmD2KEEWSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, electrons],
        descriptors=['[D+ -> K+ e+ e+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


# Instantiations, KMuE
@register_line_builder(all_lines)
@configurable
def d2kmueos_line(name='Hlt2RareCharmD2KMuEOSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, electrons, muons],
        descriptors=['[D+ -> K+ mu+ e-]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2kemuos_line(name='Hlt2RareCharmD2KEMuOSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, electrons, muons],
        descriptors=['[D+ -> K+ e+ mu-]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2kmuess_line(name='Hlt2RareCharmD2KMuESSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, electrons, muons],
        descriptors=['[D+ -> K- mu+ e+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pimuews_line(name='Hlt2RareCharmD2KMuEWSLine', prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    electrons = make_rarecharm_electrons()
    muons = make_rarecharm_muons()
    ds = make_rarecharm_d2hll_ds(
        particles=[kaons, electrons, muons],
        descriptors=['[D+ -> K+ mu+ e+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


# Instantiations, PiMuE
@register_line_builder(all_lines)
@configurable
def d2pimueos_line(name='Hlt2RareCharmD2PiMuEOSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, electrons, muons],
        descriptors=['[D+ -> pi+ mu+ e-]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2piemuos_line(name='Hlt2RareCharmD2PiEMuOSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, electrons, muons],
        descriptors=['[D+ -> pi+ e+ mu-]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pimuess_line(name='Hlt2RareCharmD2PiMuESSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions_tightSel()
    electrons = make_rarecharm_electrons_tightSel()
    muons = make_rarecharm_muons_tightSel()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, electrons, muons],
        descriptors=['[D+ -> pi- mu+ e+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def d2pimuews_line(name='Hlt2RareCharmD2piMuEWSLine', prescale=1):
    pvs = make_pvs()
    pions = make_rarecharm_pions()
    electrons = make_rarecharm_electrons()
    muons = make_rarecharm_muons()
    ds = make_rarecharm_d2hll_ds(
        particles=[pions, electrons, muons],
        descriptors=['[D+ -> pi+ mu+ e+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [ds],
        prescale=prescale,
    )


""" baryonic lines """


@register_line_builder(all_lines)
@configurable
def Lc2pmumu_line(name='Hlt2RareCharmLc2PMuMuLine', prescale=1):
    pvs = make_pvs()
    protons = make_rarecharm_protons()
    muons = make_rarecharm_muons()
    dimuons = make_rarecharm_TwoMuons(particles=muons, pvs=pvs)
    lambdas = make_rarecharm_lambdacs2pmumu(
        particles=[protons, dimuons],
        descriptors=[
            'Lambda_c+ -> J/psi(1S) p+', 'Lambda_c~- -> J/psi(1S) p~-'
        ],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [lambdas],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pmumuss_line(name='Hlt2RareCharmLc2PMuMuSSLine', prescale=1):
    pvs = make_pvs()
    protons = make_rarecharm_protons()
    muons = make_rarecharm_muons()
    dimuons = make_rarecharm_TwoMuons(particles=muons, pvs=pvs)
    lambdas = make_rarecharm_lambdacs2pmumu(
        particles=[protons, dimuons],
        descriptors=[
            'Lambda_c+ -> phi(1020) p~-', 'Lambda_c~- ->  rho(770)0 p+'
        ],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [lambdas],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pmume_line(name='Hlt2RareCharmLc2PMuELine', prescale=1):
    pvs = make_pvs()
    protons = make_rarecharm_protons()
    muons = make_rarecharm_muons()
    electrons = make_rarecharm_electrons()
    dileptons = make_rarecharm_ElectronMuon(
        particles=[muons, electrons], pvs=pvs)
    lambdas = make_rarecharm_lambdacs2pel(
        particles=[protons, dileptons],
        descriptors=[
            'Lambda_c+ -> J/psi(1S) p+', 'Lambda_c~- -> J/psi(1S) p~-',
            'Lambda_c+ -> phi(1020) p+', 'Lambda_c~- -> phi(1020) p~-'
        ],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [lambdas],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pee_line(name='Hlt2RareCharmLc2PEELine', prescale=1):
    pvs = make_pvs()
    protons = make_rarecharm_protons()
    electrons = make_rarecharm_electrons()
    lambdas = make_rarecharm_lambdacs2pel(
        particles=[protons, electrons],
        descriptors=[
            'Lambda_c+ -> J/psi(1S) p+', 'Lambda_c~- -> J/psi(1S) p~-'
        ],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [lambdas],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pmumuss_line(name='Hlt2RareCharmLc2PEESSLine', prescale=1):
    pvs = make_pvs()
    protons = make_rarecharm_protons()
    electrons = make_rarecharm_electrons()
    dielectrons = make_rarecharm_TwoElectrons(particles=electrons, pvs=pvs)
    lambdas = make_rarecharm_lambdacs2pel(
        particles=[protons, dielectrons],
        descriptors=[
            'Lambda_c+ -> phi(1020) p~-', 'Lambda_c~- ->  rho(770)0 p+'
        ],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [lambdas],
        prescale=prescale,
    )


""" Define the tagged hadronic D02HHPIPI control lines """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipipipicontrol_line(
        name='Hlt2RareCharmDstp2D0Pip_D02PiPiPiPiControlLine', prescale=0.02):
    pvs = make_pvs()
    noPID_pions = make_rarecharm_noPID_pions()
    dipions = make_rarecharm_TwoPions(
        particles=noPID_pions, pvs=pvs)  # the dimuon objects
    dzeros = make_rarecharm_d02hhmumu_dzeros(
        particles=[noPID_pions, dipions],
        descriptors=['D0 -> pi- pi+ J/psi(1S)'],
        am_min=_Mass_M_MIN_Control_HHll,
        am_max=_Mass_M_MAX_Control_HHll,
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpipipicontrol_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KPiPiPiControlLine', prescale=0.01):

    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    noPID_pions = make_rarecharm_noPID_pions()
    dipions = make_rarecharm_TwoPions(
        particles=noPID_pions, pvs=pvs)  # the dimuon objects
    dzeros = make_rarecharm_d02hhmumu_dzeros(
        particles=[kaons, noPID_pions, dipions],
        descriptors=['D0 -> K- pi+ J/psi(1S)', 'D0 -> K+ pi- J/psi(1S)'],
        am_min=_Mass_M_MIN_Control_HHll,
        am_max=_Mass_M_MAX_Control_HHll,
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kkpipicontrol_line(
        name='Hlt2RareCharmDstp2D0Pip_D02KKPiPiControlLine', prescale=0.02):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons()
    noPID_pions = make_rarecharm_noPID_pions()
    dipions = make_rarecharm_TwoPions(particles=noPID_pions, pvs=pvs)
    dzeros = make_rarecharm_d02hhmumu_dzeros(
        particles=[kaons, dipions],
        descriptors=['D0 -> K- K+ J/psi(1S)'],
        am_min=_Mass_M_MIN_Control_HHll,
        am_max=_Mass_M_MAX_Control_HHll,
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


""" Define the tagged hadronic D02HH control lines """


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2pipi_line(name='Hlt2RareCharmDstp2D0Pip_D02PiPiLine',
                                  prescale=0.02):
    pvs = make_pvs()
    pions = make_rarecharm_pions_forTwoBodyDecays()
    dzeros = make_rarecharm_d02ll_dzeros(
        particles=[pions],
        descriptors=['D0 -> pi- pi+'],
        adamass=_DMassWin_LL,
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2Kmu_line(name='Hlt2RareCharmDstp2D0Pip_D02KMuLine',
                                 prescale=1):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_forTwoBodyDecays()
    muons = make_rarecharm_muons_forTwoBodyDecays()
    dzeros = make_rarecharm_d02ll_dzeros(
        particles=[kaons, muons],
        descriptors=['D0 -> K- mu+', 'D0 -> K+ mu-'],
        adamass=_DMassWin_LL,
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeropi_dzero2kpi_line(name='Hlt2RareCharmDstp2D0Pip_D02KPiLine',
                                 prescale=0.01):
    pvs = make_pvs()
    kaons = make_rarecharm_kaons_forTwoBodyDecays()
    pions = make_rarecharm_pions_forTwoBodyDecays()
    dzeros = make_rarecharm_d02ll_dzeros(
        particles=[kaons, pions],
        descriptors=['[D0 -> K- pi+]cc'],
        adamass=_DMassWin_LL,
        pvs=pvs)
    soft_pions = make_selected_rarecharm_slowpions()
    dstars = make_dstars(
        dzeros=dzeros,
        soft_pions=soft_pions,
        descriptors=['[D*(2010)+ -> D0 pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )
