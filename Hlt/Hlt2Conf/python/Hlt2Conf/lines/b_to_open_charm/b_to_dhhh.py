###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDhh lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

all_lines = {}


@register_line_builder(all_lines)
@configurable
def BuToD0PiPiPi_D0ToKsLLHH_line(name='Hlt2B2OC_BuToD0PiPiPi_D0ToKsLLHH_Line',
                                 prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 pi+ pi- pi+', 'B- -> D0 pi+ pi- pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0PiPiPi_D0ToKsDDHH_line(name='Hlt2B2OC_BuToD0PiPiPi_D0ToKsDDHH_Line',
                                 prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 pi+ pi- pi+', 'B- -> D0 pi+ pi- pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0KPiPi_D0ToKsLLHH_Line(name='Hlt2B2OC_BuToD0KPiPi_D0ToKsLLHH_Line',
                                prescale=1):
    bachelor_pions = basic_builder.make_bachelor_pions()
    bachelor_kaons = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor_pions, bachelor_kaons],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi+ pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0KPiPi_D0ToKsDDHH_Line(name='Hlt2B2OC_BuToD0KPiPi_D0ToKsDDHH_Line',
                                prescale=1):
    bachelor_pions = basic_builder.make_bachelor_pions()
    bachelor_kaons = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor_pions, bachelor_kaons],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi+ pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


# TODO: WS equivalents
