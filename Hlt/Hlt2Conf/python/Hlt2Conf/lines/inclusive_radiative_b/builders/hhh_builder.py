###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of (HH)H builder for inclusive_radiative_b selections.
"""
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable


@configurable
def make_hhh(kstars,
             hadrons,
             pvs,
             apt_min=2.0 * GeV,
             num_neutral_tracks_max=2,
             doca_chi2_max=1000.,
             am_max=10.0 * GeV,
             mipchi2dv_max=16.,
             vtx_chi2_max=1000.,
             bpvvdchi2_min=16.,
             eta_min=2.,
             eta_max=5.,
             dira_min=0.):
    """Builds HHH particle as HH + H for inclusive_radiative_b"""
    combination_code = require_all(
        "APT > {apt_min}",
        "ANUM((ID=='KS0')|(ABSID=='Lambda0')) < {num_neutral_tracks_max}",
        "ACUTDOCACHI2({doca_chi2_max}, '')", "AM < {am_max}",
        "(ACHILD(NINTREE(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')",
        "MIPCHI2DV(PRIMARY) < {mipchi2dv_max})),1)+ANUM(((ABSID=='K+')|(ID=='KS0')|(ABSID=='Lambda0')",
        "MIPCHI2DV(PRIMARY) < {mipchi2dv_max}))) < 2").format(
            apt_min=apt_min,
            num_neutral_tracks_max=num_neutral_tracks_max,
            doca_chi2_max=doca_chi2_max,
            am_max=am_max,
            mipchi2dv_max=mipchi2dv_max)

    vertex_code = require_all("HASVERTEX", "VFASPF(VCHI2) < {vtx_chi2_max}",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "in_range({eta_min}, BPVETA(), {eta_max})",
                              "BPVDIRA() > {dira_min}").format(
                                  vtx_chi2_max=vtx_chi2_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  eta_min=eta_min,
                                  eta_max=eta_max,
                                  dira_min=dira_min)

    return ParticleCombinerWithPVs(
        particles=[kstars, hadrons],
        pvs=pvs,
        DecayDescriptors=[
            'D*(2010)+ -> K*(892)0 pi+', 'D*(2010)- -> K*(892)0 pi-'
        ],
        CombinationCut=combination_code,
        MotherCut=vertex_code)
