###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function

from Gaudi.Configuration import (ApplicationMgr, DataOnDemandSvc,
                                 appendPostConfigAction)


def assert_empty_dataondemand_service():
    assert DataOnDemandSvc().AlgMap == {}, DataOnDemandSvc().AlgMap
    assert DataOnDemandSvc().NodeMap == {}, DataOnDemandSvc().NodeMap


def setup():
    # Make sure nothing's configuring the DoD behind our back, e.g. LHCbApp
    appendPostConfigAction(assert_empty_dataondemand_service)


# TODO delete this file
