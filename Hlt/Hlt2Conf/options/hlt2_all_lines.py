###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines from reconstructed data.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_all_lines.py
"""
from __future__ import absolute_import, division, print_function
from Moore import options, run_moore
from RecoConf.reco_objects_from_file import stateProvider_with_simplified_geom
from Hlt2Conf.lines import all_lines

options.set_conds_from_testfiledb('Upgrade_MinBias_LDST')
options.set_input_from_testfiledb('Upgrade_MinBias_LDST')
options.input_raw_format = 4.3
options.evt_max = 100


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
