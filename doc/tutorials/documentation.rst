Documenting Moore
=================

This documentation is written in `reStructuredText`_ and built in to web pages by `Sphinx`_.

We follow the `Google style guide`_ (`example`_) when writing `docstrings`_ in Python.

Building the documentation locally
----------------------------------

When building the docs for the first time, the following lines of code should do the trick:

.. code-block:: sh

  . /cvmfs/lhcb.cern.ch/lib/LbEnv-testing.sh
  . /cvmfs/lhcbdev.cern.ch/nightlies/lhcb-head/Yesterday/setupSearchPath.sh
  $VIRTUAL_ENV/clone_venv moore_venv
  unset LBENV_SOURCED
  source moore_venv/bin/LbEnv.sh -r $MYSITEROOT
  pip install -r Moore/doc/requirements.txt
  pip install pydot
  pip install graphviz
  Moore/run env PYTHONHOME=$(pwd)/moore_venv make -C Moore/doc html


Once the new env is set up, it can be used again like this:

.. code-block:: sh

  . /cvmfs/lhcb.cern.ch/lib/LbEnv-testing.sh
  source moore_venv/bin/LbEnv.sh -r $MYSITEROOT
  Moore/run env PYTHONHOME=$(pwd)/moore_venv make -C Moore/doc html

.. _reStructuredText: https://docutils.sourceforge.io/rst.html
.. _Sphinx: https://www.sphinx-doc.org/en/master/
.. _Google style guide: https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
.. _docstrings: https://www.python.org/dev/peps/pep-0257/#what-is-a-docstring
.. _example: https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html#example-google
