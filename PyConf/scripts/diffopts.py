#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
import argparse
import os
import sys
from PyConf.utilities import read_options, diff_options


def exclusion(s):
    x = s.split(',', 1)
    return tuple(x) if len(x) == 2 else ('.*', x[0])


parser = argparse.ArgumentParser(description='Diff Gaudi options.')
parser.add_argument('old', help='Old options file (.py, .opts or .pkl).')
parser.add_argument('new', help='New options file (.py, .opts or .pkl).')
parser.add_argument(
    '--color',
    action='store_true',
    default=sys.stdout.isatty(),
    help='Show colored diff.')
parser.add_argument(
    '--no-color',
    dest='color',
    action='store_false',
    help='Turn off colored diff.')
parser.add_argument(
    '--git',
    dest='method',
    action='store_const',
    const='git',
    default='git',
    help='Use git for the diff (default).')
parser.add_argument(
    '--ndiff',
    dest='method',
    action='store_const',
    const='ndiff',
    help='Use difflib.ndiff for the diff.')
parser.add_argument(
    '-e',
    '--exclude',
    type=exclusion,
    action='append',
    default=[],
    help='Exclude properties based on "[component_regex,]property_regex".',
)

args = parser.parse_args()

# remove the common prefix
prefix = os.path.dirname(os.path.commonprefix([args.old, args.new]))
old_name = os.path.relpath(args.old, prefix)
new_name = os.path.relpath(args.new, prefix)

old_options = read_options(args.old)
new_options = read_options(args.new)
out = diff_options(
    old_options,
    new_options,
    old_name,
    new_name,
    exclude=args.exclude,
    method=args.method,
    color=args.color)

if out:
    print(out)
    exit(1)
