###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest

from PyConf.components import Algorithm, Tool


def test_algo_import():
    from PyConf.Algorithms import MyAlgorithm
    assert isinstance(
        MyAlgorithm(), Algorithm
    ), "importing from PyConf.Algorithms and invoking should return an Algorithm instance"

    #tools are not to be imported from algorithms
    with pytest.raises(TypeError):
        from PyConf.Algorithms import MyTool
        MyTool()

    with pytest.raises(ImportError):
        from PyConf.Algorithms import nonexistent
        nonexistent()

    # importing twice should give the same thing
    instance_id = id(MyAlgorithm)
    from PyConf.Algorithms import MyAlgorithm
    assert id(
        MyAlgorithm
    ) == instance_id, "the caching of imports does not seem to work, check the AlgorithmImportModule"

    # get the default properties from the underlying configurable
    assert (type(MyAlgorithm.getDefaultProperties()) == dict)


def test_tool_import():
    from PyConf.Tools import MyTool
    assert isinstance(MyTool(), Tool)

    #algorithms are not to be imported from tools
    with pytest.raises(TypeError):
        from PyConf.Tools import MyAlgorithm  # noqa

    with pytest.raises(ImportError):
        from PyConf.Tools import nonexistent  # noqa

    # importing twice should give the same thing
    instance_id = id(MyTool)
    from PyConf.Tools import MyTool
    assert id(
        MyTool
    ) == instance_id, "the caching of imports does not seem to work, check the ToolImportModule"

    # get the default properties from the underlying configurable
    assert (type(MyTool.getDefaultProperties()) == dict)
