###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options to profile the python configuration with gaudirun.py.

This gaudirun.py options file works by starting the profiling whenever
it is imported by gaudirun.py and stopping it in a post-config action
after all python options and ConfigurableUsers have finished executing.

In order to use it, add this to the beginning of your list of options:

    $ gaudirun.py profile-config.py options.py

To use the ``.pstats`` dump do, for example:

    $ gprof2dot -f pstats `ls -t *.pstats | head -1` | dot -Tsvg -o profile.svg

For the last example to work, the following dependencies are needed:
$ pip install gprof2dot
$ yum install graphviz

"""
import cProfile
import pstats
import StringIO
import sys
import os
import time
from datetime import datetime
from Gaudi.Configuration import appendPostConfigAction

profile = cProfile.Profile()

filename = ('profile-{:%Y%m%dT%H%M}-{}.pstats'.format(datetime.now(),
                                                      os.getpid()))


def start_profile():
    profile.enable()


def stop_profile(sortby='cumulative'):
    profile.disable()

    cpu_time = time.clock()
    sys.stderr.write('Elapsed CPU time {:.1f}\n'.format(cpu_time))

    s = StringIO.StringIO()
    ps = pstats.Stats(profile, stream=s).sort_stats(sortby)
    ps.dump_stats(filename)
    ps.print_stats(.1)
    sys.stderr.write(s.getvalue())


# Post-config action inception, or how do we make sure this is really
# the last thing to be called
appendPostConfigAction(lambda: appendPostConfigAction(stop_profile))

start_profile()
