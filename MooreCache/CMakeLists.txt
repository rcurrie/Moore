###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
gaudi_subdir(MooreCache)

gaudi_depends_on_subdirs(Hlt/Hlt1Conf
                         Phys/FunctorCore)

# Suppress compilation warnings from external packages
find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

# Import the cache creation module
include(LoKiFunctorsCache)

# Allow build in satellite (lb-dev) projects
if(TARGET MooreConfUserDB)
  set(conf_deps DEPENDS MooreConfUserDB)
endif()

# Disable LoKi-specific hacks in LoKiFunctorsCachePostActionOpts.py
# Save the old value so we don't change the behaviour elsewhere.
set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS_TMP ${LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS})
set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS "")

# Additional factories for Upgrade
loki_functors_cache(Moore_FunctorCache
                    ${REC_ROOT_DIR}/Phys/FunctorCache/options/DisableLoKiCacheFunctors.py
                    ${Moore_SOURCE_DIR}/Hlt/Moore/tests/options/default_input_and_conds_hlt1.py
                    ${Moore_SOURCE_DIR}/MooreCache/options/process_zero_events.py
                    ${Moore_SOURCE_DIR}/MooreCache/options/silence_application_manager.py
                    ${Moore_SOURCE_DIR}/Hlt/Hlt1Conf/options/hlt1_pp_default.py
                    FACTORIES FunctorFactory
                    LINK_LIBRARIES FunctorCoreLib
                    ${conf_deps}
                    SPLIT 15)

# Restore the old value
set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS ${LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS_TMP})
